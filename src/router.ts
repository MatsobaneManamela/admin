import {Routes} from '@angular/router';
import {HomepageComponent} from './app/homepage/homepage.component';
import {SigninComponent} from './app/signin/signin.component';
import {SearchResultComponent} from './app/search-result/search-result.component';
import {AddStudentComponent} from './app/add-student/add-student.component';
import {AddTeacherComponent} from './app/add-teacher/add-teacher.component';
import {ViewAllStudentComponent} from './app/view-all-student/view-all-student.component';

export const appRoutes : Routes =[
    {path: 'homepage', component : HomepageComponent},
    {path : 'login', component : SigninComponent},
    {path: 'Search/:StudentNumber', component : SearchResultComponent},
    {path: 'Add-student', component : AddStudentComponent},
    {path: 'Add-teacher', component : AddTeacherComponent},
    {path: 'View-all-student', component : ViewAllStudentComponent},
    {path: '',redirectTo: '/login', pathMatch : 'full'}

];;
