import { TestBed } from '@angular/core/testing';

import { HomeAndFirstadditionalLanguageGrade12Service } from './home-and-firstadditional-language-grade12.service';

describe('HomeAndFirstadditionalLanguageGrade12Service', () => {
  let service: HomeAndFirstadditionalLanguageGrade12Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeAndFirstadditionalLanguageGrade12Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
