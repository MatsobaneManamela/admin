import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade11} from '../Models/home-and-first-additional-language-grade11';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade11: HomeAndFirstAdditionalLanguageGrade11;

  constructor(private httpClient : HttpClient) { }

  Posthomeandfirstadditionallanguagegrade11(homeandfirstadditionallanguagegrade11: HomeAndFirstAdditionalLanguageGrade11){
    var body = JSON.stringify(homeandfirstadditionallanguagegrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade11', body, {headers : headersOption});
    }

     // get Guardians information by foriegn key
      gethomeandfirstadditionallanguagegrade11(data:any): Observable<HomeAndFirstAdditionalLanguageGrade11[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade11[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade11byID?id='+data);
      }

      //update Guardians profile

    Updatehomeandfirstadditionallanguagegrade11(id,homeandfirstadditionallanguagegrade10){
     var body = JSON.stringify(homeandfirstadditionallanguagegrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade11/'+id, body, {headers : headersOption});
      }

      Deletehomeandfirstadditionallanguagegrade11(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade11/'+id);
      }
}
