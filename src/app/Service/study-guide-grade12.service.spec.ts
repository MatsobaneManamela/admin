import { TestBed } from '@angular/core/testing';

import { StudyGuideGrade12Service } from './study-guide-grade12.service';

describe('StudyGuideGrade12Service', () => {
  let service: StudyGuideGrade12Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudyGuideGrade12Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
