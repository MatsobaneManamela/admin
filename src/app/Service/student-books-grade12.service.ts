import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade12} from '../Models/student-books-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade12: StudentBooksGrade12;
  constructor(private httpClient : HttpClient) { }

  Poststudentbooksgrade12(studentbooksgrade12: StudentBooksGrade12){
    var body = JSON.stringify(studentbooksgrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade12', body, {headers : headersOption});
    }

     // get student books information by forieng key
      getstudentbooksgrade12(data:any): Observable<StudentBooksGrade12[]>
      {
        return this.httpClient.get<StudentBooksGrade12[]>(this.rootUrl+'api/GetStudent_Books_Grade12?id='+data);
      }

      //update student books information

    Updatestudentbooksgrade12(id,studentbooksgrade12){
     var body = JSON.stringify(studentbooksgrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade12/'+id, body, {headers : headersOption});
      }

      Deletestudentbooksgrade12(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Books_Grade12/'+id);
      }
}
