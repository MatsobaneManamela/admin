import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentSubjectGrade10} from '../Models/student-subject-grade10';

@Injectable({
  providedIn: 'root'
})
export class StudentSubjectGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  studentsubjectgrade10: StudentSubjectGrade10;
  constructor(private httpClient : HttpClient) { }

  PostStudentSubjectGrade10(studentsubjectgrade10: StudentSubjectGrade10){
    var body = JSON.stringify(studentsubjectgrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade10', body, {headers : headersOption});
    }

     // get subject information by foriegn key
      getStudentSubjectGrade10(data:any): Observable<StudentSubjectGrade10[]>
      {
        return this.httpClient.get<StudentSubjectGrade10[]>(this.rootUrl+'api/GetStudent_Subject_Grade10?id='+data);
      }

      //update subject information

    UpdateStudentSubjectGrade10(id,studentsubjectgrade10){
     var body = JSON.stringify(studentsubjectgrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade10/'+id, body, {headers : headersOption});
      }

      DeleteStudentSubjectGrade10(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Subject_Grade10/'+id);
      }
}
