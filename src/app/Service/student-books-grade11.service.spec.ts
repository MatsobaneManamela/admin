import { TestBed } from '@angular/core/testing';

import { StudentBooksGrade11Service } from './student-books-grade11.service';

describe('StudentBooksGrade11Service', () => {
  let service: StudentBooksGrade11Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentBooksGrade11Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
