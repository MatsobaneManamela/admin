import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentSubjectGrade12} from '../Models/student-subject-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudentSubjectGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentsubjectgrade12: StudentSubjectGrade12;

  constructor(private httpClient : HttpClient) { }

  PostStudentSubjectGrade12(studentsubjectgrade12: StudentSubjectGrade12){
    var body = JSON.stringify(studentsubjectgrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade12', body, {headers : headersOption});
    }

     // get subject information by foriegn key
      getStudentSubjectGrade12(data:any): Observable<StudentSubjectGrade12[]>
      {
        return this.httpClient.get<StudentSubjectGrade12[]>(this.rootUrl+'api/GetStudent_Subject_Grade12?id='+data);
      }

      //update subject profile

    UpdateStudentSubjectGrade12(id,studentsubjectgrade12){
     var body = JSON.stringify(studentsubjectgrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade12/'+id, body, {headers : headersOption});
      }

      DeleteStudentSubjectGrade12(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Subject_Grade12/'+id);
      }
}
