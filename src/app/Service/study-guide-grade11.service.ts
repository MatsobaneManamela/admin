import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade11} from '../Models/student-study-guide-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade11: StudentStudyGuideGrade11;

  constructor(private httpClient : HttpClient) { }

  Poststudentstudyguidegrade11(studentstudyguidegrade11: StudentStudyGuideGrade11){
    var body = JSON.stringify(studentstudyguidegrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_StudyGuide_Grade11', body, {headers : headersOption});
    }

     // get studyGuide information by foriegn key
      getstudentstudyguidegrade11(data:any): Observable<StudentStudyGuideGrade11[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade11[]>(this.rootUrl+'api/GetStudent_StudyGuide_Grade11?id='+data);
      }

      //update studyGuide information

    Updatestudentstudyguidegrade11(id,studentstudyguidegrade11){
     var body = JSON.stringify(studentstudyguidegrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_StudyGuide_Grade11/'+id, body, {headers : headersOption});
      }

      Deletestudentstudyguidegrade11(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_StudyGuide_Grade11/'+id);
      }
}
