import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Student} from '../Models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  readonly rootUrl = "http://localhost:64567/";

  student: Student;


  constructor(private httpClient : HttpClient) { }

  PostStudent(student: Student){
    var body = JSON.stringify(student);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Students', body, {headers : headersOption});
    }

     // get Student information by forieng key
      getStudents(data:any): Observable<Student[]>
      {
        return this.httpClient.get<Student[]>(this.rootUrl+'api/Students/'+data);
      }
      getALLStudent(): Observable<Student[]>
      {
        return this.httpClient.get<Student[]>(this.rootUrl+'api/Students');
      }
      //update Student profile

    UpdateStudent(id,student){
     var body = JSON.stringify(student);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Students/'+id, body, {headers : headersOption});
      }

      DeleteStudent(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Students/'+id);
      }
}
