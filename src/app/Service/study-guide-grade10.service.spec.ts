import { TestBed } from '@angular/core/testing';

import { StudyGuideGrade10Service } from './study-guide-grade10.service';

describe('StudyGuideGrade10Service', () => {
  let service: StudyGuideGrade10Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudyGuideGrade10Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
