import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade11} from '../Models/student-books-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade11: StudentBooksGrade11;

  constructor(private httpClient : HttpClient) { }

  Poststudentbooksgrade11(studentbooksgrade11: StudentBooksGrade11){
    var body = JSON.stringify(studentbooksgrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade11', body, {headers : headersOption});
    }

     // get Guardians information by forieng key
      getstudentbooksgrade11(data:any): Observable<StudentBooksGrade11[]>
      {
        return this.httpClient.get<StudentBooksGrade11[]>(this.rootUrl+'api/GetStudent_Books_Grade11?id='+data);
      }

      //update Guardians profile

    Updatestudentbooksgrade11(id,studentbooksgrade11){
     var body = JSON.stringify(studentbooksgrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade11/'+id, body, {headers : headersOption});
      }

      Deletestudentbooksgrade11(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Books_Grade11/'+id);
      }
}
