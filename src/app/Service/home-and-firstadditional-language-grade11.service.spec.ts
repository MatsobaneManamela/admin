import { TestBed } from '@angular/core/testing';

import { HomeAndFirstadditionalLanguageGrade11Service } from './home-and-firstadditional-language-grade11.service';

describe('HomeAndFirstadditionalLanguageGrade11Service', () => {
  let service: HomeAndFirstadditionalLanguageGrade11Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeAndFirstadditionalLanguageGrade11Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
