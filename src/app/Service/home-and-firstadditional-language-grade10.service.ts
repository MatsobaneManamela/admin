import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade10} from '../Models/home-and-first-additional-language-grade10';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade10: HomeAndFirstAdditionalLanguageGrade10;
  gethomeandfirstadditionallanguagegrade10 : Subject<Array<HomeAndFirstAdditionalLanguageGrade10>> = new BehaviorSubject<Array<HomeAndFirstAdditionalLanguageGrade10>>([]);
  selectedhomeandfirstadditionallanguagegrade10 : HomeAndFirstAdditionalLanguageGrade10; 
  homeandfirstadditionallanguagegrade10list : HomeAndFirstAdditionalLanguageGrade10[];
  allhomeandfirstadditionallanguagegrade10list : HomeAndFirstAdditionalLanguageGrade10[];

  constructor(private httpClient : HttpClient) { }

  PostHomeAndFirstAdditionalLanguageGrade10(homeandfirstadditionallanguagegrade10: HomeAndFirstAdditionalLanguageGrade10){
    var body = JSON.stringify(homeandfirstadditionallanguagegrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade10', body, {headers : headersOption});
    }

     // get Guardians information by forieng key
      getHomeAndFirstAdditionalLanguageGrade10(data:any): Observable<HomeAndFirstAdditionalLanguageGrade10[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade10[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade10byID?id='+data);
      }

      //update Guardians profile

    UpdateHomeAndFirstAdditionalLanguageGrade10(id,homeandfirstadditionallanguagegrade10){
     var body = JSON.stringify(homeandfirstadditionallanguagegrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade10/'+id, body, {headers : headersOption});
      }

      DeleteHomeAndFirstAdditionalLanguageGrade10(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Home_And_FirstAdditional_Language_Grade10/'+id);
      }
}
