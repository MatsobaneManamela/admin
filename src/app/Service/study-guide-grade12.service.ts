import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade12} from '../Models/student-study-guide-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade12: StudentStudyGuideGrade12;

  constructor(private httpClient : HttpClient) { }

  Poststudentstudyguidegrade12(studentstudyguidegrade12: StudentStudyGuideGrade12){
    var body = JSON.stringify(studentstudyguidegrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_GuideBook_Grade12', body, {headers : headersOption});
    }

     // get studyguide information by forieng key
      getstudentstudyguidegrade12(data:any): Observable<StudentStudyGuideGrade12[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade12[]>(this.rootUrl+'api/GetStudent_GuideBook_Grade12?id='+data);
      }

      //update Studyguide information

    Updatestudentstudyguidegrade11(id,studentstudyguidegrade12){
     var body = JSON.stringify(studentstudyguidegrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_GuideBook_Grade12/'+id, body, {headers : headersOption});
      }

      Deletestudentstudyguidegrade12(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_GuideBook_Grade12/'+id);
      }
}
