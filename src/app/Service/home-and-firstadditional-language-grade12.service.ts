import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade12} from '../Models/home-and-first-additional-language-grade12';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade12: HomeAndFirstAdditionalLanguageGrade12;
  selectedhomeandfirstadditionallanguagegrade12 : HomeAndFirstAdditionalLanguageGrade12; 
  homeandfirstadditionallanguagegrade12list : HomeAndFirstAdditionalLanguageGrade12[];
  allhomeandfirstadditionallanguagegrade12list : HomeAndFirstAdditionalLanguageGrade12[];

  constructor(private httpClient : HttpClient) { }

  Posthomeandfirstadditionallanguagegrade12(homeandfirstadditionallanguagegrade12: HomeAndFirstAdditionalLanguageGrade12){
    var body = JSON.stringify(homeandfirstadditionallanguagegrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_firstAdditional_Language_Grade12', body, {headers : headersOption});
    }

     // get Guardians information by forieng key
      gethomeandfirstadditionallanguagegrade12(data:any): Observable<HomeAndFirstAdditionalLanguageGrade12[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade12[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade12byID?id='+data);
      }

      //update Guardians profile

    Updatehomeandfirstadditionallanguagegrade12(id,homeandfirstadditionallanguagegrade12){
     var body = JSON.stringify(homeandfirstadditionallanguagegrade12);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Home_And_firstAdditional_Language_Grade12/'+id, body, {headers : headersOption});
      }

      Deletehomeandfirstadditionallanguagegrade12(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Home_And_firstAdditional_Language_Grade12/'+id);
      }
}
