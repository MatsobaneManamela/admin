import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Administrator} from '../Models/administrator';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {
  readonly rootUrl = "http://localhost:64567/";
  administrator : Administrator;

  constructor(private httpClient : HttpClient) { }

  //Adminstrator Login Function 
  userAuthentication(UserName, Password){
    var data = "username="+UserName+"&password="+Password+"&grant_type=password";
    var reqHeader = new HttpHeaders({'Content-Type':'application/x-www-urlencoded'});
    return this.httpClient.post(this.rootUrl+'/token',data, {headers: reqHeader});
  }

}
