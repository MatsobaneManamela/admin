import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentSubjectGrade11} from '../Models/student-subject-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudentSubjectGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentsubjectgrade11: StudentSubjectGrade11;

  constructor(private httpClient : HttpClient) { }

  PostStudentSubjectGrade11(studentsubjectgrade11: StudentSubjectGrade11){
    var body = JSON.stringify(studentsubjectgrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade11', body, {headers : headersOption});
    }

     // get subject information by foriegn key
      getStudentSubjectGrade11(data:any): Observable<StudentSubjectGrade11[]>
      {
        return this.httpClient.get<StudentSubjectGrade11[]>(this.rootUrl+'api/GetStudent_Subject_Grade11?id='+data);
      }

      //update subject information

    UpdateStudentSubjectGrade11(id,studentsubjectgrade11){
     var body = JSON.stringify(studentsubjectgrade11);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Subject_Grade11/'+id, body, {headers : headersOption});
      }

      DeleteStudentSubjectGrade11(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Subject_Grade11/'+id);
      }
}
