import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Teacher} from '../Models/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  readonly rootUrl = "http://localhost:64567/";

  teacher: Teacher;
  selectedteacher : Teacher; 
  getteacher : Teacher[];
  allteacherlist : Teacher[];

  constructor(private httpClient : HttpClient) { }

  Postteacher(teacher: Teacher){
    var body = JSON.stringify(teacher);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Teachers', body, {headers : headersOption});
    }

     // get Guardians information by forieng key
      getteachers(): Observable<Teacher[]>
      {
        return this.httpClient.get<Teacher[]>(this.rootUrl+'api/Teachers/'+localStorage.getItem('TeacherID'));
      }

      getALLteachers(): Observable<Teacher[]>
      {
        return this.httpClient.get<Teacher[]>(this.rootUrl+'api/Teachers');
      }
      //update Guardians profile

    Updateteacher(id,teacher){
     var body = JSON.stringify(teacher);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Teachers/'+id, body, {headers : headersOption});
      }

      DeleteStudent(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Teachers/'+id);
      }
}
