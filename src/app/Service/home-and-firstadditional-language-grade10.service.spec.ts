import { TestBed } from '@angular/core/testing';

import { HomeAndFirstadditionalLanguageGrade10Service } from './home-and-firstadditional-language-grade10.service';

describe('HomeAndFirstadditionalLanguageGrade10Service', () => {
  let service: HomeAndFirstadditionalLanguageGrade10Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeAndFirstadditionalLanguageGrade10Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
