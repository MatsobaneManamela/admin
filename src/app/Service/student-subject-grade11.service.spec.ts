import { TestBed } from '@angular/core/testing';

import { StudentSubjectGrade11Service } from './student-subject-grade11.service';

describe('StudentSubjectGrade11Service', () => {
  let service: StudentSubjectGrade11Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentSubjectGrade11Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
