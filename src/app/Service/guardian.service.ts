import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import{Guardian} from '../Models/guardian';

@Injectable({
  providedIn: 'root'
})
export class GuardianService {
  readonly rootUrl = "http://localhost:64567/";

  guadian: Guardian;

  constructor(private httpClient : HttpClient) { }

  PostGuardians(guadian: Guardian){
    var body = JSON.stringify(guadian);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Guardians', body, {headers : headersOption});
    }

     // get Guardians information by foriegn key
      getGuardians(data:any): Observable<Guardian[]>
      {
        return this.httpClient.get<Guardian[]>(this.rootUrl+'api/GetGuardian?id='+data);
      }

      //update Guardians profile

    UpdateGuardians(id,guadian){
     var body = JSON.stringify(guadian);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Guardians/'+id, body, {headers : headersOption});
      }

      DeleteGuardians(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Guardians/'+id);
      }
}
