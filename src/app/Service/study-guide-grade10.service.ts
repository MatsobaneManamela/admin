import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade10} from '../Models/student-study-guide-grade10';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade10: StudentStudyGuideGrade10;

  constructor(private httpClient : HttpClient) { }

  Poststudentstudyguidegrade10(studentstudyguidegrade10: StudentStudyGuideGrade10){
    var body = JSON.stringify(studentstudyguidegrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_StudyGuide_Grade10', body, {headers : headersOption});
    }

     // get studyGuide information by foriegn key
      getstudentstudyguidegrade10(data:any): Observable<StudentStudyGuideGrade10[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade10[]>(this.rootUrl+'api/GetStudent_StudyGuide_Grade10?id='+data);
      }

      //update studyguide information

    Updatestudentstudyguidegrade10(id,studentstudyguidegrade10){
     var body = JSON.stringify(studentstudyguidegrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_StudyGuide_Grade10/'+id, body, {headers : headersOption});
      }

      Deletestudentstudyguidegrade10(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_StudyGuide_Grade10/'+id);
      }
}
