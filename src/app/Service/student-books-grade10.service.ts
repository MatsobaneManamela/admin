import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade10} from '../Models/student-books-grade10';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade10: StudentBooksGrade10;

  constructor(private httpClient : HttpClient) { }

  Poststudentbooksgrade10(studentbooksgrade10: StudentBooksGrade10){
    var body = JSON.stringify(studentbooksgrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade10', body, {headers : headersOption});
    }

     // get Guardians information by foriegn key
      getstudentbooksgrade10(data:any): Observable<StudentBooksGrade10[]>
      {
        return this.httpClient.get<StudentBooksGrade10[]>(this.rootUrl+'api/GetStudent_Books_Grade10?id='+data);
      }

      //update Guardians profile

    Updatestudentbooksgrade10(id,studentbooksgrade10){
     var body = JSON.stringify(studentbooksgrade10);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Student_Books_Grade10/'+id, body, {headers : headersOption});
      }

      Deletestudentbooksgrade10(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Student_Books_Grade10/'+id);
      }
}
