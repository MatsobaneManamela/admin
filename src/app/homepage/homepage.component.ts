import { Component, OnInit } from '@angular/core';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private route : Router,  private toaster : ToastrService) { }

  ngOnInit(): void {
  }
  Logout(){
    localStorage.clear();
    this.route.navigate(['/login']);
    this.toaster.success('signed out Sucessfully')
  }
}
