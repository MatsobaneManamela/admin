import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {appRoutes} from '../router';
import {ToastrModule, ToastContainerModule} from 'ngx-toastr';
import {AuthGuard} from './Auth/auth.guard.service';
import { SigninComponent } from './signin/signin.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { SearchResultComponent } from './search-result/search-result.component';
import {TeacherService} from './Service/teacher.service';
import {StudentService} from './Service/student.service';
import { ViewAllStudentComponent } from './view-all-student/view-all-student.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    HomepageComponent,
    AddStudentComponent,
    AddTeacherComponent,
    SearchResultComponent,
    ViewAllStudentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    ToastrModule.forRoot({ positionClass: 'inline' }),
    ToastContainerModule,
  ],
  providers: [AuthGuard,TeacherService,StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
