import { Component, OnInit } from '@angular/core';
import {Student} from '../Models/student';
import {StudentService} from '../Service/student.service';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-all-student',
  templateUrl: './view-all-student.component.html',
  styleUrls: ['./view-all-student.component.css']
})
export class ViewAllStudentComponent implements OnInit {
  student: Student;
  studentarray: Student[]
  constructor(private route : Router,  private toaster : ToastrService,private studentservice : StudentService) { }

  ngOnInit(): void {
    this.studentservice.getALLStudent().subscribe((data:any) => { this.studentarray = data});
  }
  Logout(){
    localStorage.clear();
    this.route.navigate(['/login']);
    this.toaster.success('signed out Sucessfully')
  }


}
