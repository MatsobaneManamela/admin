import { Component, OnInit } from '@angular/core';
import {HomeAndFirstAdditionalLanguageGrade10} from '../Models/home-and-first-additional-language-grade10';
import {HomeAndFirstadditionalLanguageGrade10Service} from '../Service/home-and-firstadditional-language-grade10.service';
import {HomeAndFirstAdditionalLanguageGrade11} from '../Models/home-and-first-additional-language-grade11';
import {HomeAndFirstadditionalLanguageGrade11Service} from '../Service/home-and-firstadditional-language-grade11.service';
import {HomeAndFirstAdditionalLanguageGrade12} from '../Models/home-and-first-additional-language-grade12';
import {HomeAndFirstadditionalLanguageGrade12Service} from '../Service/home-and-firstadditional-language-grade12.service';
import {StudentBooksGrade10} from '../Models/student-books-grade10';
import {StudentBooksGrade10Service} from '../Service/student-books-grade10.service';
import {StudentBooksGrade11} from '../Models/student-books-grade11';
import {StudentBooksGrade11Service} from '../Service/student-books-grade11.service';
import {StudentBooksGrade12} from '../Models/student-books-grade12';
import {StudentBooksGrade12Service} from '../Service/student-books-grade12.service';
import {StudentStudyGuideGrade10} from '../Models/student-study-guide-grade10';
import {StudyGuideGrade10Service} from '../Service/study-guide-grade10.service';
import {StudentStudyGuideGrade11} from '../Models/student-study-guide-grade11';
import {StudyGuideGrade11Service} from'../Service/study-guide-grade11.service';
import {StudentStudyGuideGrade12} from '../Models/student-study-guide-grade12';
import {StudyGuideGrade12Service} from '../Service/study-guide-grade12.service';
import {StudentSubjectGrade10} from '../Models/student-subject-grade10';
import {StudentSubjectGrade10Service} from '../Service/student-subject-grade10.service';
import {StudentSubjectGrade11} from '../Models/student-subject-grade11';
import {StudentSubjectGrade11Service} from '../Service/student-subject-grade11.service';
import {StudentSubjectGrade12} from '../Models/student-subject-grade12';
import {StudentSubjectGrade12Service} from '../Service/student-subject-grade12.service';
import {Guardian}  from '../Models/guardian';
import {GuardianService} from '../Service/guardian.service';
import {Student} from '../Models/student';
import {StudentService} from '../Service/student.service';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { map,mergeMap} from 'rxjs/operators';
const URL = 'http://localhost:3000/';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  Grade10 : string;
  Grade11 : string;
  Grade12 : string;
  homeandefirstadditionallanguagegrade10 : HomeAndFirstAdditionalLanguageGrade10;
  homeandefirstadditionallanguagegrade11 :HomeAndFirstAdditionalLanguageGrade11;
  homeandefirstadditionallanguagegrade12 :  HomeAndFirstAdditionalLanguageGrade12;
  studentbooksgrade10 :StudentBooksGrade10;
  studentbooksgrade11 :StudentBooksGrade11;
  studentbooksgrade12 :StudentBooksGrade12;
  studentstudyguidegrade10 : StudentStudyGuideGrade10;
  studentstudyguidegrade11 : StudentStudyGuideGrade11;
  studentstudyguidegrade12 : StudentStudyGuideGrade12;
  studentsubjectgrade10 : StudentSubjectGrade10;
  studentsubjectgrade11 : StudentSubjectGrade11;
  studentsubjectgrade12 : StudentSubjectGrade12;
  guadian : Guardian;
  student: Student;
  fileToUpload : File = null;
  fileupd : File = null;
  selectedFile: File = null;
  fd = new FormData();
  Imageurl : string = "/assets/Mathematical Literacy P2 Nov 2014 Memo Eng.pdf";
  constructor(private route : Router,  private toaster : ToastrService,private http : HttpClient ,
    private homeandefirstadditionallanguagegrade10service : HomeAndFirstadditionalLanguageGrade10Service,
    private homeandefirstadditionallanguagegrade11service : HomeAndFirstadditionalLanguageGrade11Service,
    private homeandefirstadditionallanguagegrade12service : HomeAndFirstadditionalLanguageGrade12Service,
    private studentbooksgrade10service: StudentBooksGrade10Service,
    private studentbooksgrade11service :StudentBooksGrade11Service,
    private studentbooksgrade12service : StudentBooksGrade12Service,
    private studentguidegrade10service : StudyGuideGrade10Service,
    private studentguidegrade11service :StudyGuideGrade11Service,
    private studentguidegrade12service : StudyGuideGrade12Service,
    private studentsubjectgrade10service:StudentSubjectGrade10Service,
    private studentsubjectgrade11service :StudentSubjectGrade11Service,
    private studentsubjectgrade12service:StudentSubjectGrade12Service,
    private studentservice : StudentService,
    private guadianservice : GuardianService,
    ) { }

  ngOnInit(): void {
    this.resetForm();
    this.Grade10 = 'Grade10';
    this.Grade11 = 'Grade11';
    this.Grade12 = 'Grade12';
  }
  resetForm(form? : NgForm){

    if(form != null)
    form.reset();
    this.student={
      StudentNumber : 0,
      Name: '',
      Surname : '',
      IDNumber: 0, 
      Gender : '',
      MobileNumbers: '',
      HomeAddress : '',
      Suburb : '',
      City : '',
      Province: '', 
      ZIP : '',
      Grade : '', 
      Password : '',
      Email : ''

    }
    this.guadian={
      ID : 0,
      StudentNumber: +this.student.StudentNumber,
       Name : '',
       Surname : '',
      IDNumber : '',
       Gender : '',
       MobileNumber: '',
       HomeNumber: '',
       PhysicalAddress: '',
       Suburb: '',
       City: '',
       Province: '',
       ZIP : '',
       Relation : ''
    }
    this.homeandefirstadditionallanguagegrade10={
      LanguageID : 0,
      StudentNumber: +this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.homeandefirstadditionallanguagegrade11={
      LanguageID : 0,
      StudentNumber: +this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.homeandefirstadditionallanguagegrade12={
      LanguageID : 0,
      StudentNumber: this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.studentbooksgrade10={
      StudentBookID : 0,
      StudentNumber: +this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentbooksgrade11={
      StudentID : 0,
      StudentNumber: +this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentbooksgrade12={
      StudentBookID : 0,
      StudentNumber: +this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentstudyguidegrade10={
      StudyGuideID : 0,
      StudentNumber : +this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentstudyguidegrade11={
      StudyGuideID : 0,
      StudentNumber : this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentstudyguidegrade12={
      StudyGuideID : 0,
      StudentNumber : this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentsubjectgrade10={
      SubjectID : 0,
      StudentNumber : +this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
    this.studentsubjectgrade11={
      SubjectID : 0,
      StudentNumber : +this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
    this.studentsubjectgrade12={
      SubjectID : 0,
      StudentNumber : +this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
  }
  Logout(){
    localStorage.clear();
    this.route.navigate(['/login']);
    this.toaster.success('signed out Sucessfully')
  }
  /*createFormData(event) {
    this.selectedFile = <File>event.target.files[0];
    this.fd.append('photo', this.selectedFile, this.selectedFile.name);
  }*/

  createFormData(event) {
    this.fileupd = <File>event.target.files[0];
    this.fd.append('pdf', this.fileupd, this.fileupd.name);
  }

  handleFileInput(file : FileList){

    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event:any) =>{
    this.Imageurl = event.target.result;
    }
  reader.readAsDataURL(this.fileToUpload);
  console.log(this.fileToUpload.name);

  }
  onSubmit(form? : NgForm){
         // NODE JS SERVER 
         this.http.post(URL, this.fd).pipe(map((res:Response) => res.json())).subscribe(
          (success) => {
               console.log(this.fd);
         },
         (error) => alert(error));
// C# BACK 
    //form.value.StudentNumber = +form.value.StudentNumber;
    

    if (this.student.Grade == 'Grade10'){
      this.studentservice.PostStudent(form.value)
      .subscribe((data:any) => {
          if (data.Succeeded == true)
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.PostGuardians(form.value)
       .subscribe((data:any) => {
           if (data.Succeeded == true)
          this.resetForm(form);
          // this.toaster.success('your Service details are successfully saved');
          // location.reload();
          }); 
    this.studentbooksgrade10service.Poststudentbooksgrade10(form.value)
    .subscribe((data:any) => {
        if (data.Succeeded == true)
          this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
  
  this.studentguidegrade10service.Poststudentstudyguidegrade10(form.value)
    .subscribe((data:any) => {
        if (data.Succeeded == true)
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.studentsubjectgrade10service.PostStudentSubjectGrade10(form.value)
    .subscribe((data:any) => {
        if (data.Succeeded == true)
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.homeandefirstadditionallanguagegrade10service.PostHomeAndFirstAdditionalLanguageGrade10(form.value)
    .subscribe((data:any) => {
        if (data.Succeeded == true)
       this.resetForm(form);
        this.toaster.success('your information are successfully saved');
        location.reload();
       }); 
      }
     else if (this.student.Grade == 'Grade11'){
      this.studentservice.PostStudent(form.value)
      .subscribe((data:any) => {
          if (data.Succeeded == true)
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.PostGuardians(form.value)
       .subscribe((data:any) => {
           if (data.Succeeded == true)
          this.resetForm(form);
          // this.toaster.success('your Service details are successfully saved');
          // location.reload();
          }); 
        this.studentbooksgrade11service.Poststudentbooksgrade11(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true) {
              console.log(form);
              this.resetForm(form);
            }
           // this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
      
      this.studentguidegrade11service.Poststudentstudyguidegrade11(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
           // this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
           this.studentsubjectgrade11service.PostStudentSubjectGrade11(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
           // this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
           this.homeandefirstadditionallanguagegrade11service.Posthomeandfirstadditionallanguagegrade11(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
           this.toaster.success('your information are successfully saved');
            location.reload();
           }); 
      }
     else if (this.student.Grade == 'Grade12'){
      this.studentservice.PostStudent(form.value)
      .subscribe((data:any) => {
          if (data.Succeeded == true)
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.PostGuardians(form.value)
       .subscribe((data:any) => {
           if (data.Succeeded == true)
          this.resetForm(form);
          // this.toaster.success('your Service details are successfully saved');
          // location.reload();
          }); 
          
        this.studentbooksgrade12service.Poststudentbooksgrade12(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
           // this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
      
      this.studentguidegrade12service.Poststudentstudyguidegrade12(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
            //this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
           this.studentsubjectgrade12service.PostStudentSubjectGrade12(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
           // this.toaster.success('your Service details are successfully saved');
           // location.reload();
           }); 
           this.homeandefirstadditionallanguagegrade12service.Posthomeandfirstadditionallanguagegrade12(form.value)
        .subscribe((data:any) => {
            if (data.Succeeded == true)
           this.resetForm(form);
            this.toaster.success('your information are successfully saved');
           location.reload();
           }); 
      }
  }
  
  
  
}
