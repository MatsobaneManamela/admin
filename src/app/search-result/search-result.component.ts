import { Component, OnInit } from '@angular/core';
import {HomeAndFirstAdditionalLanguageGrade10} from '../Models/home-and-first-additional-language-grade10';
import {HomeAndFirstadditionalLanguageGrade10Service} from '../Service/home-and-firstadditional-language-grade10.service';
import {HomeAndFirstAdditionalLanguageGrade11} from '../Models/home-and-first-additional-language-grade11';
import {HomeAndFirstadditionalLanguageGrade11Service} from '../Service/home-and-firstadditional-language-grade11.service';
import {HomeAndFirstAdditionalLanguageGrade12} from '../Models/home-and-first-additional-language-grade12';
import {HomeAndFirstadditionalLanguageGrade12Service} from '../Service/home-and-firstadditional-language-grade12.service';
import {StudentBooksGrade10} from '../Models/student-books-grade10';
import {StudentBooksGrade10Service} from '../Service/student-books-grade10.service';
import {StudentBooksGrade11} from '../Models/student-books-grade11';
import {StudentBooksGrade11Service} from '../Service/student-books-grade11.service';
import {StudentBooksGrade12} from '../Models/student-books-grade12';
import {StudentBooksGrade12Service} from '../Service/student-books-grade12.service';
import {StudentStudyGuideGrade10} from '../Models/student-study-guide-grade10';
import {StudyGuideGrade10Service} from '../Service/study-guide-grade10.service';
import {StudentStudyGuideGrade11} from '../Models/student-study-guide-grade11';
import {StudyGuideGrade11Service} from'../Service/study-guide-grade11.service';
import {StudentStudyGuideGrade12} from '../Models/student-study-guide-grade12';
import {StudyGuideGrade12Service} from '../Service/study-guide-grade12.service';
import {StudentSubjectGrade10} from '../Models/student-subject-grade10';
import {StudentSubjectGrade10Service} from '../Service/student-subject-grade10.service';
import {StudentSubjectGrade11} from '../Models/student-subject-grade11';
import {StudentSubjectGrade11Service} from '../Service/student-subject-grade11.service';
import {StudentSubjectGrade12} from '../Models/student-subject-grade12';
import {StudentSubjectGrade12Service} from '../Service/student-subject-grade12.service';
import {Guardian}  from '../Models/guardian';
import {GuardianService} from '../Service/guardian.service';
import {Student} from '../Models/student';
import {StudentService} from '../Service/student.service';
import {ActivatedRoute, Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { map,mergeMap} from 'rxjs/operators';
import { Subscription } from 'rxjs';
const URL = 'http://localhost:3000/files';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  Grade10 : string;
  Grade11 : string;
  Grade12 : string;
  homeandefirstadditionallanguagegrade10 : HomeAndFirstAdditionalLanguageGrade10;
  homeandefirstadditionallanguagegrade11 :HomeAndFirstAdditionalLanguageGrade11;
  homeandefirstadditionallanguagegrade12 :  HomeAndFirstAdditionalLanguageGrade12;
  studentbooksgrade10 :StudentBooksGrade10;
  studentbooksgrade11 :StudentBooksGrade11;
  studentbooksgrade12 :StudentBooksGrade12;
  studentstudyguidegrade10 : StudentStudyGuideGrade10;
  studentstudyguidegrade11 : StudentStudyGuideGrade11;
  studentstudyguidegrade12 : StudentStudyGuideGrade12;
  studentsubjectgrade10 : StudentSubjectGrade10;
  studentsubjectgrade11 : StudentSubjectGrade11;
  studentsubjectgrade12 : StudentSubjectGrade12;
  guadian : Guardian;
  student: Student;
  fileToUpload : File = null;
  fileupd : File = null;
  selectedFile: File = null;
  fd = new FormData();
  Imageurl : string = "/assets/default_profile_image.png";
  StudentNumbers: any;
  sub : Subscription;
  exam:any;
  studentarray: Student[];


  constructor(private router: ActivatedRoute,private route : Router,  private toaster : ToastrService,private http : HttpClient ,
    private homeandefirstadditionallanguagegrade10service : HomeAndFirstadditionalLanguageGrade10Service,
    private homeandefirstadditionallanguagegrade11service : HomeAndFirstadditionalLanguageGrade11Service,
    private homeandefirstadditionallanguagegrade12service : HomeAndFirstadditionalLanguageGrade12Service,
    private studentbooksgrade10service: StudentBooksGrade10Service,
    private studentbooksgrade11service :StudentBooksGrade11Service,
    private studentbooksgrade12service : StudentBooksGrade12Service,
    private studentguidegrade10service : StudyGuideGrade10Service,
    private studentguidegrade11service :StudyGuideGrade11Service,
    private studentguidegrade12service : StudyGuideGrade12Service,
    private studentsubjectgrade10service:StudentSubjectGrade10Service,
    private studentsubjectgrade11service :StudentSubjectGrade11Service,
    private studentsubjectgrade12service:StudentSubjectGrade12Service,
    private studentservice : StudentService,
    private guadianservice : GuardianService,) { }

  ngOnInit(): void {
    this.resetForm();
    this.Grade10 = 'Grade10';
    this.Grade11 = 'Grade11';
    this.Grade12 = 'Grade12';
    
    this.sub=this.router.params.subscribe(params=>{

      this.StudentNumbers=params['StudentNumber'];
      
      if(this.StudentNumbers){
        this.studentservice.getStudents(this.StudentNumbers).subscribe((data:any) => { this.student = data});
        this.studentbooksgrade10service.getstudentbooksgrade10(this.StudentNumbers).subscribe((data : any)=>{this.studentbooksgrade10 = data});
        this.studentbooksgrade11service.getstudentbooksgrade11(this.StudentNumbers).subscribe((data:any) =>{this.studentbooksgrade11 = data});
        this.studentbooksgrade12service.getstudentbooksgrade12(this.StudentNumbers).subscribe((data : any)=>{ this.studentbooksgrade12 =data});
        this.studentguidegrade10service.getstudentstudyguidegrade10(this.StudentNumbers).subscribe((data: any)=>{this.studentstudyguidegrade10 = data });
        this.studentguidegrade11service.getstudentstudyguidegrade11(this.StudentNumbers).subscribe((data : any)=>{this.studentstudyguidegrade11 = data});
        this.studentguidegrade12service.getstudentstudyguidegrade12(this.StudentNumbers).subscribe((data : any)=>{this.studentstudyguidegrade12 = data});
        this.studentsubjectgrade10service.getStudentSubjectGrade10(this.StudentNumbers).subscribe((data : any)=>{ this.studentsubjectgrade10 = data});
        this.studentsubjectgrade11service.getStudentSubjectGrade11(this.StudentNumbers).subscribe((data : any)=>{this.studentsubjectgrade11 = data});
        this.studentsubjectgrade12service.getStudentSubjectGrade12(this.StudentNumbers).subscribe((data : any)=>{this.studentsubjectgrade12 = data});
        this.homeandefirstadditionallanguagegrade10service.getHomeAndFirstAdditionalLanguageGrade10(this.StudentNumbers).subscribe((data : any)=>{this.homeandefirstadditionallanguagegrade10 = data});
        this.homeandefirstadditionallanguagegrade11service.gethomeandfirstadditionallanguagegrade11(this.StudentNumbers).subscribe((data : any)=>{this.homeandefirstadditionallanguagegrade11 =data});
        this.homeandefirstadditionallanguagegrade12service.gethomeandfirstadditionallanguagegrade12(this.StudentNumbers).subscribe((data : any)=>{this.homeandefirstadditionallanguagegrade12 = data});
        this.guadianservice.getGuardians(this.StudentNumbers).subscribe((data: any) => {

          this.guadian = data;
           
        }, (error) =>{
        
          if(error.status==200){
            location.reload();
          }
          if (this.exam=== undefined || this.exam== null) {
                console.log(error);
            }
        });

      }else{
        console.log(Error); 
      }
    });

  }

  resetForm(form? : NgForm){

    if(form != null)
    form.reset();
    this.student={
      StudentNumber : 0,
      Name: '',
      Surname : '',
      IDNumber: 0, 
      Gender : '',
      MobileNumbers: '',
      HomeAddress : '',
      Suburb : '',
      City : '',
      Province: '', 
      ZIP : '',
      Grade : '', 
      Password : '',
      Email : ''

    }
    this.guadian={
      ID : 0,
      StudentNumber: this.student.StudentNumber,
       Name : '',
       Surname : '',
      IDNumber : '',
       Gender : '',
       MobileNumber: '',
       HomeNumber: '',
       PhysicalAddress: '',
       Suburb: '',
       City: '',
       Province: '',
       ZIP : '',
       Relation : ''
    }
    this.homeandefirstadditionallanguagegrade10={
      LanguageID : 0,
      StudentNumber: this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.homeandefirstadditionallanguagegrade11={
      LanguageID : 0,
      StudentNumber:this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.homeandefirstadditionallanguagegrade12={
      LanguageID : 0,
      StudentNumber: this.student.StudentNumber,
      HomeLanguageBook1 :'',
      HomeLanguageBook2 : '',
      HomeLanguageBook3 :'',
      FirstAdditionalLanguageBook1: '',
      FirstAdditionalLanguageBook2 : '',
      FirstAdditionalLanguageBook3 :''
    }
    this.studentbooksgrade10={
      StudentBookID : 0,
      StudentNumber: this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentbooksgrade11={
      StudentID : 0,
      StudentNumber: this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentbooksgrade12={
      StudentBookID : 0,
      StudentNumber: this.student.StudentNumber,
      StudentBook1 : '',
      StudentBook2 : '',
      StudentBook3 : '',
      StudentBook4 : '',
      StudentBook5 : ''
    }
    this.studentstudyguidegrade10={
      StudyGuideID : 0,
      StudentNumber : this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentstudyguidegrade11={
      StudyGuideID : 0,
      StudentNumber : this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentstudyguidegrade12={
      StudyGuideID : 0,
      StudentNumber : this.student.StudentNumber,
      StudyGuideBook1 : '',
      StudyGuideBook2 : '',
      StudyGuideBook3 : '',
      StudyGuideBook4 : '',
      StudyGuideBook5 : '',
      StudyGuideBook6 : '',
      StudyGuideBook7 : ''
    }
    this.studentsubjectgrade10={
      SubjectID : 0,
      StudentNumber : this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
    this.studentsubjectgrade11={
      SubjectID : 0,
      StudentNumber : this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
    this.studentsubjectgrade12={
      SubjectID : 0,
      StudentNumber : this.student.StudentNumber,
      Subject1 : '',
      Subject2 : '',
      Subject3 : '',
      Subject4 : '',
      Subject5 : '',
      Subject6 : '',
      Subject7 : ''
    }
  }
  Logout(){
    localStorage.clear();
    this.route.navigate(['/login']);
    this.toaster.success('signed out Sucessfully')
  }
  createFormData(event) {
    this.fileupd = <File>event.target.files[0];
    this.fd.append('photo', this.fileupd, this.fileupd.name);
  }

  handleFileInput(file : FileList){

    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event:any) =>{
    this.Imageurl = event.target.result;
    }
  reader.readAsDataURL(this.fileToUpload);
  console.log(this.fileToUpload.name);
  //this.bookservice.selectedbooking.Image =  this.fileToUpload.name;
  }
  UpdateUser(form? : NgForm){
         // NODE JS SERVER 
         this.http.post(URL, this.fd).pipe(map((res:Response) => res.json())).subscribe(
          (success) => {
               
         },
         (error) => alert(error));
// C# BACK 

    if (this.student.Grade == 'Grade10'){
      this.studentservice.UpdateStudent(form.value.StudentNumber,form.value)
      .subscribe(data => {
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.UpdateGuardians(form.value.ID,form.value)
       .subscribe(data => {
           
          this.resetForm(form);
          }); 
    this.studentbooksgrade10service.Updatestudentbooksgrade10(form.value.StudentBookID,form.value)
    .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
  
  this.studentguidegrade10service.Updatestudentstudyguidegrade10(form.value.StudyGuideID,form.value)
    .subscribe(data => {
        
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.studentsubjectgrade10service.UpdateStudentSubjectGrade10(form.value.SubjectID,form.value)
       .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.homeandefirstadditionallanguagegrade10service.UpdateHomeAndFirstAdditionalLanguageGrade10(form.value.LanguageID,form.value)
    .subscribe(data => { 
       this.resetForm(form);
        this.toaster.success('your information are successfully saved');
        location.reload();
       }); 
      }
     else if (this.student.Grade == 'Grade11'){
      this.studentservice.UpdateStudent(form.value.StudentNumber,form.value)
      .subscribe(data => {
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.UpdateGuardians(form.value.ID,form.value)
       .subscribe(data => {
           
          this.resetForm(form);
          }); 
    this.studentbooksgrade11service.Updatestudentbooksgrade11(form.value.StudentID,form.value)
    .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
  
  this.studentguidegrade11service.Updatestudentstudyguidegrade11(form.value.StudyGuideID,form.value)
    .subscribe(data => {
        
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.studentsubjectgrade11service.UpdateStudentSubjectGrade11(form.value.SubjectID,form.value)
       .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.homeandefirstadditionallanguagegrade11service.Updatehomeandfirstadditionallanguagegrade11(form.value.LanguageID,form.value)
    .subscribe(data => { 
       this.resetForm(form);
        this.toaster.info('your information are successfully saved');
        location.reload();
       }); 
      }
     else if (this.student.Grade == 'Grade12'){
      this.studentservice.UpdateStudent(form.value.StudentNumber,form.value)
      .subscribe(data => {
         this.resetForm(form);
         // this.toaster.success('your Service details are successfully saved');
         // location.reload();
         });
       this.guadianservice.UpdateGuardians(form.value.ID,form.value)
       .subscribe(data => {
           
          this.resetForm(form);
          }); 
    this.studentbooksgrade12service.Updatestudentbooksgrade12(form.value.StudentBookID,form.value)
    .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
  
  this.studentguidegrade12service.Updatestudentstudyguidegrade11(form.value.StudyGuideID,form.value)
    .subscribe(data => {
        
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.studentsubjectgrade12service.UpdateStudentSubjectGrade12(form.value.SubjectID,form.value)
       .subscribe(data => {
       this.resetForm(form);
       // this.toaster.success('your Service details are successfully saved');
       // location.reload();
       }); 
       this.homeandefirstadditionallanguagegrade12service.Updatehomeandfirstadditionallanguagegrade12(form.value.LanguageID,form.value)
    .subscribe(data => { 
       this.resetForm(form);
        this.toaster.success('your information are successfully saved');
        location.reload();
       }); 
      }
  }
}
