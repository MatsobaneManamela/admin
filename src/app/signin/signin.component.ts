import { Component, OnInit } from '@angular/core';
import {AdministratorService} from '../Service/administrator.service';
import {Administrator} from'../Models/administrator';
import { HttpErrorResponse } from '@angular/common/http';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  administrator: Administrator;
  isLoginError : boolean = false;

  constructor(private administratorservice:AdministratorService, private route : Router, private toaster : ToastrService) { }

  ngOnInit(): void {
    this.resetForm();
  }
  resetForm(form? : NgForm){

    if(form != null)
    form.reset();
    this.administrator = {
      ID : 0,
      Email : '',
      Password : '' 
    }
  }

  OnSubmit(Email,Password){
 
    this.administratorservice.userAuthentication(Email,Password).subscribe((data : any)=>{
    localStorage.setItem('userToken', data.access_token);
    this.route.navigate(['/homepage']);
    this.toaster.success('Welcome');
    },
       
    (err : HttpErrorResponse)=>{
    this.isLoginError = true;
    this.toaster.error('incorrect password & Email');
    });
    } 

}
