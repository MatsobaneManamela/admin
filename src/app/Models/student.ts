export class Student {

     StudentNumber: number;
     Name: string; 
     Surname : string;
     IDNumber: number; 
     Gender : string;
     MobileNumbers: string;
     HomeAddress :string;
     Suburb :string;
     City :string;
     Province: string; 
     ZIP : string;
     Grade :string; 
     Password : string;
     Email : string; 
}
