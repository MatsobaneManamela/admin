export class StudentSubjectGrade11 {
    SubjectID : number;
    StudentNumber : number;
    Subject1 : string;
    Subject2 : string;
    Subject3 : string;
    Subject4 : string;
    Subject5 : string;
    Subject6 : string;
    Subject7 : string;
}
