export class StudentStudyGuideGrade10 {
    StudyGuideID : number;
    StudentNumber : number;
    StudyGuideBook1 : string;
    StudyGuideBook2 : string;
    StudyGuideBook3 : string;
    StudyGuideBook4 : string;
    StudyGuideBook5 : string;
    StudyGuideBook6 : string;
    StudyGuideBook7 : string;
}
