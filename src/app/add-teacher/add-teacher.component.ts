import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Teacher} from '../Models/teacher';
import {TeacherService} from '../Service/teacher.service';

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {
  teacher : Teacher;
  teachers : Teacher[];

  constructor(private teacherservice : TeacherService,private route : Router, private toaster : ToastrService) { }

  ngOnInit(): void {

    this.teacherservice.getteachers();
    this.teacherservice.getALLteachers().subscribe((data:any) => { this.teachers = data});
    this.resetForm();

  }
  resetForm(form? : NgForm){

    if(form != null)
    form.reset();
    this.teacher = {
     TeacherID:0,
     Name: '',
     Surname : '',
     IDNumber: '',
     Gender: '',
     Email: '',
     Password : '',
     Position : '',
     MobileNumber: '',
     HomeNumber : '',
     Specialization : '' 
    }
  }

  Logout(){
    localStorage.clear();
    this.route.navigate(['/login']);
    this.toaster.success('signed out Sucessfully')
  }
  onSubmit(form? : NgForm){

// C# BACK END
    this.teacherservice.Postteacher(form.value)
    .subscribe((data:any) => {
        if (data.Succeeded == true)
       this.resetForm(form);
        this.toaster.success('your Service details are successfully saved');
        location.reload();
       }); 
  }

  UpdateTeacher(form? : NgForm){

    this.teacherservice.Updateteacher(form.value.TeacherID, form.value)
    .subscribe(data => {
      this.resetForm(form);
      this.toaster.info('Record Updated Successfully');
      location.reload();
    ;
    })
  }

  showForEdit(teacher : Teacher){
    this.teacher = Object.assign({}, teacher);
  }


  onDelete(id : number){
    if(confirm("are you sure you want to delete?")==true){
      this.teacherservice.DeleteStudent(id).subscribe(x =>{
        this.teacherservice.getteachers();
      this.toaster.warning('Deleted Successfully');
      }
    )
    }
  }
}
